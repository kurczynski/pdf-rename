module pdf-rename

go 1.21

require (
	github.com/fsnotify/fsnotify v1.7.0
	github.com/ledongthuc/pdf v0.0.0-20220302134840-0c2507a12d80
	github.com/spf13/pflag v1.0.5
)

require golang.org/x/sys v0.4.0 // indirect
