package document

import (
	"bytes"
	"fmt"
	"github.com/ledongthuc/pdf"
	"io"
	"log/slog"
	"os"
	"regexp"
	"strconv"
	"strings"
	"time"
)

const (
	Pdf = "pdf"
)

type FileFmt struct {
	rootName   string // Root name of the document, the document's time is suffixed on this
	timeLayout string // Layout used to parse the time in the document
	timeRegex  string // Regex used to match the time in the document
	extension  string
}

func CreateFilename(date time.Time, fFmt FileFmt) (string, error) {
	filename := fmt.Sprintf("%s-%s.%s", date.Format(ISO8601NoZ), fFmt.rootName, fFmt.extension)

	slog.Info("Created filename", "filename", filename)

	return filename, nil
}

func CreateNextFname(currentFname string, fFmt FileFmt) (string, error) {
	base := strings.Trim(currentFname, fmt.Sprintf(".%s", fFmt.extension))

	ts, after, ok := strings.Cut(base, fFmt.rootName)

	if !ok {
		return "", fmt.Errorf("")
	}

	var count int
	var err error

	if len(after) > 0 {
		count, err = strconv.Atoi(after[1:])

		if err != nil {
			return "", err
		}
	}

	count++

	filename := fmt.Sprintf("%s%s-%d.%s", ts, fFmt.rootName, count, fFmt.extension)

	slog.Info("Created filename", "filename", filename)

	return filename, nil
}

func ExtractDate(docText string, fInfo FileFmt) (time.Time, error) {
	// FIXME: Don't keep recompiling the regexes
	r, err := regexp.Compile(fInfo.timeRegex)

	if err != nil {
		return time.Time{}, err
	}

	token := r.FindString(docText)

	if token == "" {
		slog.Error("Could not find a date in the document using the given layout", "layout", fInfo.timeLayout)
	} else {
		slog.Info("Found document date", "date", token)
	}

	t, err := time.ParseInLocation(fInfo.timeLayout, token, time.Local)

	if err != nil {
		return time.Time{}, err
	}

	return t, nil
}

func ReadFile(path string) (string, error) {
	f, r, err := pdf.Open(path)

	defer f.Close()

	if err != nil {
		return "", err
	}

	var buf bytes.Buffer
	b, err := r.GetPlainText()

	if err != nil {
		return "", err
	}

	_, err = buf.ReadFrom(b)

	if err != nil {
		return "", err
	}

	if err != nil {
		return "", err
	}

	return buf.String(), nil
}

func MoveDocument(src string, dest string) error {
	fin, err := os.OpenFile(src, os.O_RDWR, 0660)
	defer func(fin *os.File) {
		err := fin.Close()
		if err != nil {
			slog.Error("Failed to close file", "file", fin.Name())
		}

		slog.Info("Closed file", "file", fin.Name())
	}(fin)

	if err != nil {
		return err
	}

	slog.Debug("Opened source file", "file", fin.Name())

	fout, err := os.OpenFile(dest, os.O_CREATE|os.O_WRONLY, 0660)
	defer func(fout *os.File) {
		err := fout.Close()
		if err != nil {
			slog.Error("Failed to close file", "file", fout.Name())
		}
		slog.Info("Closed file", "file", fout.Name())
	}(fout)

	if err != nil {
		return err
	}

	slog.Debug("Opened destination file", "file", fout.Name())

	_, err = io.Copy(fout, fin)

	if err != nil {
		return err
	}

	slog.Info("Copied file", "src", fin.Name(), "dest", fout.Name())

	err = os.Remove(src)

	if err != nil {
		return err
	}

	slog.Info("Removed file", "file", src)

	return nil
}
