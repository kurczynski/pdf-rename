package document

import "testing"

func TestCreateNextFname(t *testing.T) {
	type args struct {
		currentFname string
		fFmt         FileFmt
	}
	tests := []struct {
		name    string
		args    args
		want    string
		wantErr bool
	}{
		{
			name: "Should increment no digits",
			args: args{
				currentFname: "1970-01-01-home-depot.pdf",
				fFmt: FileFmt{
					rootName:   "home-depot",
					timeLayout: "",
					timeRegex:  "",
					extension:  "pdf",
				},
			},
			want: "1970-01-01-home-depot-1.pdf",
		},
		{
			name: "Should increment single digit",
			args: args{
				currentFname: "1970-01-01-home-depot-1.pdf",
				fFmt: FileFmt{
					rootName:   "home-depot",
					timeLayout: "",
					timeRegex:  "",
					extension:  "pdf",
				},
			},
			want: "1970-01-01-home-depot-2.pdf",
		},
		{
			name: "Should increment double digits",
			args: args{
				currentFname: "1970-01-01-home-depot-10.pdf",
				fFmt: FileFmt{
					rootName:   "home-depot",
					timeLayout: "",
					timeRegex:  "",
					extension:  "pdf",
				},
			},
			want: "1970-01-01-home-depot-11.pdf",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := CreateNextFname(tt.args.currentFname, tt.args.fFmt)
			if (err != nil) != tt.wantErr {
				t.Errorf("CreateNextFname() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("CreateNextFname() got = %v, want %v", got, tt.want)
			}
		})
	}
}
