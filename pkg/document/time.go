package document

import (
	"time"
)

const (
	ISO8601NoZ = time.DateOnly + "T" + time.TimeOnly

	MiddleEndianNumeric12Hour      = "01/02/06 03:04 PM"
	MiddleEndianNumeric12HourRegex = "[0-9]{2}/[0-9]{2}/[0-9]{2} +[0-9]{2}:[0-9]{2} +(AM|PM)"

	MiddleEndianFull      = "January 02, 2006"
	MiddleEndianFullRegex = "(January|February|March|April|May|June|July|August|September|October|November|December) [0-9]{2}, [0-9]{4}"
)
