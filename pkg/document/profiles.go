package document

var (
	HomeDepotInStore = FileFmt{
		rootName:   "home-depot",
		timeLayout: MiddleEndianNumeric12Hour,
		timeRegex:  MiddleEndianNumeric12HourRegex,
		extension:  Pdf,
	}
	HomeDepotOnline = FileFmt{
		rootName:   "home-depot",
		timeLayout: MiddleEndianFull,
		timeRegex:  MiddleEndianFullRegex,
		extension:  Pdf,
	}
)
