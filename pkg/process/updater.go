package process

import (
	"fmt"
	"log/slog"
	"os"
	"path/filepath"
	"pdf-rename/pkg/document"
)

type Updater struct {
	directory string
	fileFmt   document.FileFmt
	monitor   *Monitor
}

func NewUpdater(directory string, fInfo document.FileFmt, monitor *Monitor, file *bool) *Updater {
	return &Updater{
		directory: directory,
		monitor:   monitor,
		fileFmt:   fInfo,
	}
}

func (u *Updater) Update() {
	for {
		select {
		case fpath := <-u.monitor.FChan:
			u.updateFilename(fpath)
		}
	}
}

func (u *Updater) updateFilename(fpath string) {
	slog.Info("Updating old filename", "file", fpath)

	if u.monitor.Busy.Exists(fpath) {
		slog.Debug("File is busy, will not update", "file", fpath)

		return
	}

	u.monitor.Busy.Add(fpath)
	defer u.monitor.Busy.Remove(fpath)

	var fname string

	slog.Info("Reading file", "file", fpath)

	text, err := document.ReadFile(fpath)

	if os.IsNotExist(err) {
		slog.Error("Failed to read document", "file", fpath, "err", err)

		return
	}

	date, err := document.ExtractDate(text, u.fileFmt)

	if err != nil {
		slog.Error("Failed to extract date from document", "err", err)

		return
	}

	fname, err = document.CreateFilename(date, u.fileFmt)

	if err != nil {
		slog.Error("Failed to create filename", "err", err)

		return
	}

	newFpath := fmt.Sprintf("%s%c%s", filepath.Dir(fpath), os.PathSeparator, fname)

	if fpath == newFpath {
		slog.Warn("Source filepath and destination filepath are the same", "filepath", fpath)

		return
	}

	slog.Info("Moving file", "src", fpath, "dest", newFpath)

	err = document.MoveDocument(fpath, newFpath)

	if err != nil {
		slog.Error("Failed to move document", "src", fpath, "dest", newFpath, "err", err)

		return
	}

	u.monitor.Renamed.Add(newFpath)
}
