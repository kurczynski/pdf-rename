package process

import (
	"github.com/fsnotify/fsnotify"
	"log/slog"
	"os"
	"sync"
	"time"
)

var fileHistories = map[string][]os.FileInfo{}

type Monitor struct {
	FChan   chan string
	Renamed SafeSet
	Busy    SafeSet
}

type Watcher struct {
	monitor *Monitor
}

type FileHistory struct {
	fileInfo os.FileInfo
	histTs   time.Time
}

type Set interface {
	Add(string)
	Exists(string) bool
	Remove(string)
}

type SafeSet struct {
	mu   sync.Mutex
	keys map[string]struct{}
}

func NewSafeSet() SafeSet {
	return SafeSet{
		mu:   sync.Mutex{},
		keys: make(map[string]struct{}),
	}
}

func (s *SafeSet) Add(key string) {
	defer s.mu.Unlock()
	s.mu.Lock()

	s.keys[key] = struct{}{}
}

func (s *SafeSet) Exists(key string) bool {
	_, ok := s.keys[key]

	return ok
}

func (s *SafeSet) Remove(key string) {
	defer s.mu.Unlock()
	s.mu.Lock()

	delete(s.keys, key)
}

func NewWatcher(monitor *Monitor) *Watcher {
	return &Watcher{
		monitor: monitor,
	}
}

func (w *Watcher) Watch(path string) {
	watcher, err := fsnotify.NewWatcher()
	defer func(watcher *fsnotify.Watcher) {
		err := watcher.Close()
		if err != nil {
			slog.Error("Failed to close file watcher for path", "path", path, "err", err)
		}

		slog.Info("Closed file watcher for path", "path", path)
	}(watcher)

	if err != nil {
		slog.Error("Failed to create watcher for path", "path", path, "err", err)

		return
	}

	slog.Info("Watching for new files under path", "path", path)

	err = watcher.Add(path)

	if err != nil {
		slog.Error("Failed to add watcher to path", "path", path, "err", err)

		return
	}

	for {
		select {
		case event, ok := <-watcher.Events:
			if !ok {
				return
			}

			fpath := event.Name

			slog.Info("Received file watcher event", "op", event.Op, "name", fpath)

			// TODO: Check if the filename is formatted correctly, skip if it is
			if event.Has(fsnotify.Rename) || event.Has(fsnotify.Remove) || w.monitor.Busy.Exists(fpath) {
				slog.Debug("Skipping file", "file", fpath)

				continue
			}

			go handleStableFile(fpath, w.monitor)
		case err, ok := <-watcher.Errors:
			if !ok {
				return
			}

			slog.Error("An error occurred while watching files in path", "path", path, "err", err)
		}
	}
}

func handleStableFile(fpath string, monitor *Monitor) {
	slog.Debug("Checking if file is being written to", "file", fpath)

	if monitor.Busy.Exists(fpath) {
		return
	}

	monitor.Busy.Add(fpath)
	defer monitor.Busy.Remove(fpath)

	fd, err := os.Open(fpath)
	defer fd.Close()

	if err != nil {
		slog.Error("err", err)

		return
	}

	var size, prevSize int64

	for {
		fInfo, err := fd.Stat()

		if err != nil {
			slog.Error("err", err)
		}

		size = fInfo.Size()

		slog.Debug("Current file size", "file", fInfo.Name(), "size", size)

		if size > 0 && size == prevSize {
			monitor.FChan <- fpath

			break
		}

		prevSize = size

		time.Sleep(100 * time.Millisecond)
	}
}
