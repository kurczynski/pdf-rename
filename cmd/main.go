package main

import (
	"github.com/ledongthuc/pdf"
	flag "github.com/spf13/pflag"
	"log/slog"
	"os"
	"pdf-rename/pkg/document"
	"pdf-rename/pkg/process"
)

func main() {
	// TODO: Match existing file by pattern for renaming
	path := flag.StringP("path", "p", ".", "the path to watch for new files")
	failDupes := flag.BoolP("fail-duplicates", "d", false, "asdf")
	// ext := flag.StringP("extension", "e", "pdf", "the file extension to add to renamed files")

	handlerOpts := &slog.HandlerOptions{
		AddSource:   true,
		Level:       slog.LevelDebug,
		ReplaceAttr: nil,
	}

	logger := slog.New(slog.NewTextHandler(os.Stdout, handlerOpts))
	slog.SetDefault(logger)

	flag.Parse()

	pdf.DebugOn = true

	monitor := &process.Monitor{
		FChan:   make(chan string),
		Renamed: process.NewSafeSet(),
		Busy:    process.NewSafeSet(),
	}

	updater := process.NewUpdater(*path, document.HomeDepotInStore, monitor, failDupes)
	watcher := process.NewWatcher(monitor)

	go watcher.Watch(*path)

	updater.Update()
}
